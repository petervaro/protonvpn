## INFO ##
## INFO ##

COVERAGE_DIR="coverage-reports";

mkdir -p "$COVERAGE_DIR";
find . -maxdepth 1 -iname '*.lst' |
while read REPORT;
do
    mv --force "$REPORT" "$COVERAGE_DIR";
done;
