/* INFO **
** INFO */

module mocks.configurations;

private:

import std.file : SpanMode;

import pseudo.range : InputRangeMock;

public:

import pseudo.io : FileMock;

struct DirEntry
{
public:
    string name;
    bool   isFile;
}

alias DirIterator = InputRangeMock!DirEntry;

DirIterator delegate(string, SpanMode) dirEntries;

string delegate(string) expandTilde;

FileMock delegate(string, string) File;
