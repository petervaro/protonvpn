/* INFO **
** INFO */

module configurations;

private:

import std.path   : baseName;
import std.file   : FileException,
                    SpanMode;
import std.stdio  : writeln;
import std.regex  : ctRegex,
                    matchFirst,
                    matchAll,
                    Captures;
import std.string : endsWith;

version (unittest)
    import mocks.configurations : DirEntry,
                                  DirIterator,
                                  dirEntries,
                                  expandTilde,
                                  File,
                                  FileMock;
else
{
    import std.file  : dirEntries;
    import std.path  : expandTilde;
    import std.stdio : File;
}

import globals             : openVpnConfigPath;
import error               : ErrorConstructor;
import servers.connections : Connection;
import servers.protocols   : Protocol;


/*----------------------------------------------------------------------------*/
const fileNamePattern = ctRegex!(
    r"^((?P<via>[A-Za-z]{2})-)?"        ~
    r"(?P<target>[A-Za-z]{2})"          ~
    r"(-(?P<free>free))?"               ~
    r"(-(?P<index>\d{2,3}))?"           ~
    r"(-(?P<tor>tor))?"                 ~
    r"\.protonvpn\.com\.(tcp(443)?|udp(1194)?)\.ovpn$");

const remoteIpAddressPattern = ctRegex!(
    r"remote\s*(?P<ip>\d+(\.\d+){3})\s*(?P<port>\d+)");


/*----------------------------------------------------------------------------*/
void
traverse(ref Configuration[] configurations,
         string              protocol,
         string              connection)
{
    string path = expandTilde(
        openVpnConfigPath ~ "/" ~ protocol ~ "/" ~ connection);

    foreach (dirEntry; path.dirEntries(SpanMode.shallow))
    {
        if (!dirEntry.isFile)
            continue;

        Captures!string match;

        auto fileName = dirEntry.name.baseName;
        match = fileName.matchFirst(fileNamePattern);
        if (!match)
            continue;

        Configuration configuration;
        configuration.path       = path;
        configuration.name       = fileName;

        protocolSwitch:
        switch (protocol)
        {
            static foreach (member; __traits(allMembers, Protocol))
                static if (__traits(getMember, Protocol, member))
                {
                    case __traits(getMember, Protocol, member):
                        configuration.protocol = __traits(getMember,
                                                          Protocol,
                                                          member);
                        break protocolSwitch;
                }

            default:
                assert (false);
        }

        connectionSwitch:
        switch (connection)
        {
            static foreach (member; __traits(allMembers, Connection))
                static if (__traits(getMember, Connection, member))
                {
                    case __traits(getMember, Connection, member):
                        configuration.connection = __traits(getMember,
                                                            Connection,
                                                            member);
                        break connectionSwitch;
                }

            default:
                assert (false);
        }

        auto via    = match["via"];
        auto target = match["target"];
        auto free   = match["free"];
        auto index  = match["index"];
        auto tor    = match["tor"];

        target = target && target.length ? target : null;
        if (connection == "server" &&
            via == "us")
        {
            configuration.target = "us";
            configuration.state  = target;
        }
        else
        {
            configuration.target = target;
            configuration.via    = via && via.length ? via : null;
        }
        configuration.free       = free && free.length ? free : null;
        configuration.index      = index && index.length ? index : null;
        configuration.tor        = tor && tor.length ? tor : null;

        auto file = File(dirEntry.name, "r");
        scope(exit)
            file.close;

        string line;
        while ((line = file.readln) !is null)
        {
            match = line.matchFirst(remoteIpAddressPattern);
            if (match)
                configuration.addresses ~= Address(match["ip"], match["port"]);
        }

        configurations ~= configuration;
    }
}
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
unittest
{
    import std.exception : assertThrown;

    expandTilde = path => path;
    DirIterator di;
    FileMock    fm;

    dirEntries = delegate (d, m) { throw new FileException("error"); };

    {
        Configuration[] configurations;
        assertThrown!FileException(traverse(configurations, null, null));
        assert (!configurations.length);
    }

    dirEntries = (d, m) => di;

    {
        DirEntry de = {isFile: false};
        di.empty = false;
        di.popFront = this_ => this_.empty = true;
        di.front = de;

        Configuration[] configurations;
        traverse(configurations, null, null);
        assert (!configurations.length);
    }
    {
        DirEntry de = {name: "non-matching-filename", isFile: true};
        di.empty = false;
        di.popFront = this_ => this_.empty = true;
        di.front = de;

        Configuration[] configurations;
        traverse(configurations, null, null);
        assert (!configurations.length);
    }

    File = delegate (f, m) => fm;
    fm.readln = cast(string)null;

    {
        auto fileName = "xy.protonvpn.com.tcp.ovpn";
        DirEntry de = {name: fileName, isFile: true};
        di.empty = false;
        di.popFront = this_ => this_.empty = true;
        di.front = de;

        Configuration[] configurations;
        traverse(configurations, "tcp", "country");
        assert (configurations.length == 1);
        with (configurations[0])
        {
            assert (path == openVpnConfigPath ~ "/tcp/country");
            assert (name == fileName);
            assert (protocol == "tcp");
            assert (connection == "country");
            assert (target == "xy");
            assert (state is null);
            assert (via is null);
            assert (free is null);
            assert (index is null);
            assert (tor is null);
            assert (addresses is null);
        }
    }
    {
        size_t i = 0;
        auto lines = ["not matched", "remote 0.0.0.0 1111", "not matched"];
        fm.readln = () => i >= lines.length ? null : lines[i++];

        auto fileName = "aa-bb-free-12-tor.protonvpn.com.udp.ovpn";
        DirEntry de = {name: fileName, isFile: true};
        di.empty = false;
        di.popFront = this_ => this_.empty = true;
        di.front = de;

        Configuration[] configurations;
        traverse(configurations, "udp", "server");
        assert (configurations.length == 1);
        with (configurations[0])
        {
            assert (path == openVpnConfigPath ~ "/udp/server");
            assert (name == fileName);
            assert (protocol == "udp");
            assert (connection == "server");
            assert (target == "bb");
            assert (state is null);
            assert (via == "aa");
            assert (free == "free");
            assert (index == "12");
            assert (tor == "tor");
            assert (addresses.length == 1);
            with (addresses[0])
            {
                assert (host == "0.0.0.0");
                assert (port == "1111");
            }
        }
    }
    {
        auto fileName = "us-ak-155.protonvpn.com.tcp.ovpn";
        DirEntry de = {name: fileName, isFile: true};
        di.empty = false;
        di.popFront = this_ => this_.empty = true;
        di.front = de;

        Configuration[] configurations;
        traverse(configurations, "tcp", "server");
        assert (configurations.length == 1);
        with (configurations[0])
        {
            assert (path == openVpnConfigPath ~ "/tcp/server");
            assert (name == fileName);
            assert (protocol == "tcp");
            assert (connection == "server");
            assert (target == "us");
            assert (state == "ak");
            assert (via is null);
            assert (free is null);
            assert (index == "155");
            assert (tor is null);
            assert (!addresses.length);
        }
    }
}

public:

/*----------------------------------------------------------------------------*/
class TraverseError: Exception { mixin ErrorConstructor; }


/*----------------------------------------------------------------------------*/
struct Address
{
public:
    string host;
    string port;
}


/*----------------------------------------------------------------------------*/
struct Configuration
{
public:
    string     path;
    string     name;
    Protocol   protocol;
    Connection connection;
    string     target;
    string     state;
    string     via;
    string     free;
    string     index;
    string     tor;
    Address[]  addresses;

    /*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    pragma (inline)
    bool
    match(ref Configuration rhs)
    {
        /* LHS configuration is matching RHS if all of the defined releavant
           properties of the former are the same as the latter */
        static foreach (member; ["protocol",
                                 "connection",
                                 "target",
                                 "state",
                                 "via",
                                 "free",
                                 "tor"])
            if (__traits(getMember, this, member) &&
                __traits(getMember, this, member) != __traits(getMember, rhs, member))
                    return false;

        return true;
    }
}
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
unittest
{
    {
        Configuration lhs = {protocol: Protocol.tcp};
        Configuration rhs = {protocol: Protocol.tcp};
        assert (lhs.match(rhs));
    }
    {
        Configuration lhs = {path: Protocol.tcp, name: "n1"};
        Configuration rhs = {path: Protocol.udp, name: "n2"};
        assert (lhs.match(rhs));
    }
    {
        Configuration lhs = {connection: Connection.country,
                             target: "t",
                             index: "01"};
        Configuration rhs = {connection: Connection.server,
                             target: "t",
                             index: "01"};
        assert (!lhs.match(rhs));
    }
    {
        Configuration lhs = {target: "t"};
        Configuration rhs = {connection: Connection.server,
                             target: "t",
                             index: "01"};
        assert (lhs.match(rhs));
    }
    {
        Configuration lhs = {connection: Connection.secureCore};
        Configuration rhs = {connection: Connection.secureCore,
                             target: "t",
                             index: "01"};
        assert (lhs.match(rhs));
    }
}


/*----------------------------------------------------------------------------*/
// TODO: `getConfigurations` should be an InputRange (as well as `traverse`)
void
getConfigurations(ref Configuration[] configurations)
{
    static foreach (protocol; __traits(allMembers, Protocol))
        static if (__traits(getMember, Protocol, protocol))
            static foreach (connection; __traits(allMembers, Connection))
                static if (__traits(getMember, Connection, connection))
                    try
                        traverse(configurations,
                                 __traits(getMember, Protocol, protocol),
                                 __traits(getMember, Connection, connection));
                    catch (FileException error)
                    {
                        if (error.msg.endsWith("Permission denied"))
                            throw new TraverseError(
                                "Cannot access configuration files " ~
                                "(permission denied)");
                        else if (error.msg.endsWith("No such file or directory"))
                            writeln(
                                "Have you forgot to add the " ~
                                __traits(getMember, Protocol, protocol) ~
                                "/" ~
                                __traits(getMember, Connection, connection) ~
                                " configuration files?");
                        else
                            throw new TraverseError(
                                "Cannot read configuration files: " ~
                                error.msg);
                    }
}
