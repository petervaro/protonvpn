/* INFO **
** INFO */

module version_;

private:

import std.string : strip;

public:

const string version_ = mixin('"' ~ import("VERSION").strip ~ '"');
