/* INFO **
** INFO */

module servers.countries;

private:

import error         : ErrorConstructor;
import servers.error : ServerError;

public:

/*----------------------------------------------------------------------------*/
class InvalidCountry: ServerError { mixin ErrorConstructor; }


/*----------------------------------------------------------------------------*/
enum CountryShort: string
{
    australia     = "au",
    austria       = "at",
    belgium       = "be",
    brazil        = "br",
    canada        = "ca",
    chechRepublic = "cz",
    denmark       = "dk",
    finland       = "fi",
    france        = "fr",
    germany       = "de",
    hongKong      = "hk",
    iceland       = "is",
    india         = "in",
    ireland       = "ie",
    israel        = "il",
    italy         = "it",
    japan         = "jp",
    korea         = "kr",
    luxembourg    = "lu",
    netherlands   = "nl",
    newZealand    = "nz",
    norway        = "no",
    poland        = "pl",
    portugal      = "pt",
    singapore     = "sg",
    southAfrica   = "za",
    spain         = "es",
    sweden        = "se",
    switzerland   = "ch",
    unitedKingdom = "uk",
    unitedStates  = "us",
}


/*----------------------------------------------------------------------------*/
enum CountryLong: string
{
    australia     = "australia",
    austria       = "austria",
    belgium       = "belgium",
    brazil        = "brazil",
    canada        = "canada",
    chechRepublic = "chech-republic",
    denmark       = "denmark",
    finland       = "finland",
    france        = "france",
    germany       = "germany",
    hongKong      = "hong-kong",
    iceland       = "iceland",
    india         = "india",
    ireland       = "ireland",
    israel        = "israel",
    italy         = "italy",
    japan         = "japan",
    korea         = "korea",
    luxembourg    = "luxembourg",
    netherlands   = "netherlands",
    newZealand    = "new-zealand",
    norway        = "norway",
    poland        = "poland",
    portugal      = "portugal",
    singapore     = "singapore",
    southAfrica   = "south-africa",
    spain         = "spain",
    sweden        = "sweden",
    switzerland   = "switzerland",
    unitedKingdom = "united-kingdom",
    unitedStates  = "united-states",
}


/*----------------------------------------------------------------------------*/
enum CountryHumanReadable: string
{
    australia     = "Australia",
    austria       = "Austria",
    belgium       = "Belgium",
    brazil        = "Brazil",
    canada        = "Canada",
    chechRepublic = "Chech Republic",
    denmark       = "Denmark",
    finland       = "Finland",
    france        = "France",
    germany       = "Germany",
    hongKong      = "Hong Kong",
    iceland       = "Iceland",
    india         = "India",
    ireland       = "Ireland",
    israel        = "Israel",
    italy         = "Italy",
    japan         = "Japan",
    korea         = "Korea",
    luxembourg    = "Luxembourg",
    netherlands   = "Netherlands",
    newZealand    = "New Zealand",
    norway        = "Norway",
    poland        = "Poland",
    portugal      = "Portugal",
    singapore     = "Singapore",
    southAfrica   = "South Africa",
    spain         = "Spain",
    sweden        = "Sweden",
    switzerland   = "Switzerland",
    unitedKingdom = "United Kingdom",
    unitedStates  = "United States of America",
}


/*----------------------------------------------------------------------------*/
void
countryLongToShort(string     longForm,
                   ref string shortForm)
{
    switch_:
    switch (longForm)
    {
        static foreach (member; __traits(allMembers, CountryLong))
        {
            case __traits(getMember, CountryLong, member):
                shortForm = __traits(getMember, CountryShort, member);
                break switch_;
        }

        default:
            throw new InvalidCountry("Unknown country: " ~ longForm);
    }
}
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
unittest
{
    import std.exception : assertThrown;

    {
        string shortForm;
        assertThrown!InvalidCountry(countryLongToShort(null, shortForm));
    }
    {
        string shortForm;
        assertThrown!InvalidCountry(countryLongToShort("invalid", shortForm));
    }
    {
        string shortForm;
        countryLongToShort(CountryLong.australia, shortForm);
        assert (shortForm == CountryShort.australia);
    }
}


/*----------------------------------------------------------------------------*/
void
countryShortToLong(string     shortForm,
                   ref string longForm)
{
    switch_:
    switch (shortForm)
    {
        static foreach (member; __traits(allMembers, CountryShort))
        {
            case __traits(getMember, CountryShort, member):
                longForm = __traits(getMember, CountryLong, member);
                break switch_;
        }

        default:
            throw new InvalidCountry("Unknown country code: " ~ shortForm);
    }
}
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
unittest
{
    import std.exception : assertThrown;

    {
        string name;
        assertThrown!InvalidCountry(countryShortToLong(null, name));
    }
    {
        string name;
        assertThrown!InvalidCountry(countryShortToLong("invalid", name));
    }
    {
        string name;
        countryShortToLong("au", name);
        assert (name == "australia");
    }
}


/*----------------------------------------------------------------------------*/
void
countryShortToHumanReadable(string     shortForm,
                            ref string humanReadableForm)
{
    switch_:
    switch (shortForm)
    {
        static foreach (member; __traits(allMembers, CountryShort))
        {
            case __traits(getMember, CountryShort, member):
                humanReadableForm = __traits(
                    getMember, CountryHumanReadable, member);
                break switch_;
        }

        default:
            throw new InvalidCountry("Unknown country code: " ~ shortForm);
    }
}
