/* INFO **
** INFO */

module servers.protocols;

public:

enum Protocol: string
{
    undefined = null,
    tcp       = "tcp",
    udp       = "udp",
}
