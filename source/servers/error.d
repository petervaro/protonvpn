/* INFO **
** INFO */

module servers.error;

private:

import error : ErrorConstructor;

public:

/*----------------------------------------------------------------------------*/
class ServerError: Exception { mixin ErrorConstructor; }
