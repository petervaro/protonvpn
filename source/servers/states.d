/* INFO **
** INFO */

module servers.states;

private:

import error         : ErrorConstructor;
import servers.error : ServerError;

public:

/*----------------------------------------------------------------------------*/
class InvalidState: ServerError { mixin ErrorConstructor; }


/*----------------------------------------------------------------------------*/
enum StateShort
{
    alabama       = "al",
    alaska        = "ak",
    arizona       = "az",
    arkansas      = "ar",
    california    = "ca",
    colorado      = "co",
    connecticut   = "ct",
    delaware      = "de",
    florida       = "fl",
    georgia       = "ga",
    hawaii        = "hi",
    idaho         = "id",
    illinois      = "il",
    indiana       = "in",
    iowa          = "ia",
    kansas        = "ks",
    kentucky      = "ky",
    louisiana     = "la",
    maine         = "me",
    maryland      = "md",
    massachusetts = "ma",
    michigan      = "mi",
    minnesota     = "mn",
    mississippi   = "ms",
    montana       = "mt",
    nebraska      = "ne",
    nevada        = "nv",
    newHampshire  = "nh",
    newJersey     = "nj",
    newMexico     = "nm",
    newYork       = "ny",
    northCarolina = "nc",
    northDakota   = "nd",
    ohio          = "oh",
    oklahoma      = "ok",
    oregon        = "or",
    pennsylvania  = "pa",
    rhodeIsland   = "ri",
    southCarolina = "sc",
    southDakota   = "sd",
    tennessee     = "tn",
    texas         = "tx",
    utah          = "ut",
    vermont       = "vt",
    virginia      = "va",
    washington    = "wa",
    westVirginia  = "wv",
    wisconsin     = "wi",
    wyoming       = "wy",
}


/*----------------------------------------------------------------------------*/
enum StateLong
{
    alabama       = "alabama",
    alaska        = "alaska",
    arizona       = "arizona",
    arkansas      = "arkansas",
    california    = "california",
    colorado      = "colorado",
    connecticut   = "connecticut",
    delaware      = "delaware",
    florida       = "florida",
    georgia       = "georgia",
    hawaii        = "hawaii",
    idaho         = "idaho",
    illinois      = "illinois",
    indiana       = "indiana",
    iowa          = "iowa",
    kansas        = "kansas",
    kentucky      = "kentucky",
    louisiana     = "louisiana",
    maine         = "maine",
    maryland      = "maryland",
    massachusetts = "massachusetts",
    michigan      = "michigan",
    minnesota     = "minnesota",
    mississippi   = "mississippi",
    montana       = "montana",
    nebraska      = "nebraska",
    nevada        = "nevada",
    newHampshire  = "new-hampshire",
    newJersey     = "new-jersey",
    newMexico     = "new-mexico",
    newYork       = "new-york",
    northCarolina = "north-carolina",
    northDakota   = "north-dakota",
    ohio          = "ohio",
    oklahoma      = "oklahoma",
    oregon        = "oregon",
    pennsylvania  = "pennsylvania",
    rhodeIsland   = "rhode-island",
    southCarolina = "south-carolina",
    southDakota   = "south-dakota",
    tennessee     = "tennessee",
    texas         = "texas",
    utah          = "utah",
    vermont       = "vermont",
    virginia      = "virginia",
    washington    = "washington",
    westVirginia  = "west-virginia",
    wisconsin     = "wisconsin",
    wyoming       = "wyoming",
}


/*----------------------------------------------------------------------------*/
enum StateHumanReadable
{
    alabama       = "Alabama",
    alaska        = "Alaska",
    arizona       = "Arizona",
    arkansas      = "Arkansas",
    california    = "California",
    colorado      = "Colorado",
    connecticut   = "Connecticut",
    delaware      = "Delaware",
    florida       = "Florida",
    georgia       = "Georgia",
    hawaii        = "Hawaii",
    idaho         = "Idaho",
    illinois      = "Illinois",
    indiana       = "Indiana",
    iowa          = "Iowa",
    kansas        = "Kansas",
    kentucky      = "Kentucky",
    louisiana     = "Louisiana",
    maine         = "Maine",
    maryland      = "Maryland",
    massachusetts = "Massachusetts",
    michigan      = "Michigan",
    minnesota     = "Minnesota",
    mississippi   = "Mississippi",
    montana       = "Montana",
    nebraska      = "Nebraska",
    nevada        = "Nevada",
    newHampshire  = "New Hampshire",
    newJersey     = "New Jersey",
    newMexico     = "New Mexico",
    newYork       = "New York",
    northCarolina = "North Carolina",
    northDakota   = "North Dakota",
    ohio          = "Ohio",
    oklahoma      = "Oklahoma",
    oregon        = "Oregon",
    pennsylvania  = "Pennsylvania",
    rhodeIsland   = "Rhode Island",
    southCarolina = "South Carolina",
    southDakota   = "South Dakota",
    tennessee     = "Tennessee",
    texas         = "Texas",
    utah          = "Utah",
    vermont       = "Vermont",
    virginia      = "Virginia",
    washington    = "Washington",
    westVirginia  = "West Virginia",
    wisconsin     = "Wisconsin",
    wyoming       = "Wyoming",
}


/*----------------------------------------------------------------------------*/
void
stateLongToShort(string     longForm,
                 ref string shortForm)
{
    switch_:
    switch (longForm)
    {
        static foreach (member; __traits(allMembers, StateLong))
        {
            case __traits(getMember, StateLong, member):
                shortForm = __traits(getMember, StateShort, member);
                break switch_;
        }

        default:
            throw new InvalidState("Unknown state: " ~ longForm);
    }
}


/*----------------------------------------------------------------------------*/
void
stateShortToLong(string     shortForm,
                 ref string longForm)
{
    switch_:
    switch (shortForm)
    {
        static foreach (member; __traits(allMembers, StateShort))
        {
            case __traits(getMember, StateShort, member):
                longForm = __traits(getMember, StateLong, member);
                break switch_;
        }

        default:
            throw new InvalidState("Unknown state code: " ~ shortForm);
    }
}


/*----------------------------------------------------------------------------*/
void
stateShortToHumanReadable(string     shortForm,
                          ref string humanReadableForm)
{
    switch_:
    switch (shortForm)
    {
        static foreach (member; __traits(allMembers, StateShort))
        {
            case __traits(getMember, StateShort, member):
                humanReadableForm = __traits(
                    getMember, StateHumanReadable, member);
                break switch_;
        }

        default:
            throw new InvalidState("Unknown state code: " ~ shortForm);
    }
}
