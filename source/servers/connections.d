/* INFO **
** INFO */

module servers.connections;

public:

enum Connection: string
{
    undefined  = null,
    secureCore = "secure-core",
    country    = "country",
    server     = "server",
}
