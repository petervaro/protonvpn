/* INFO **
** INFO */

module command.connect.arguments;

private:

import std.concurrency : Generator;
import std.conv        : to,
                         ConvException;
import std.format      : format;

import globals             : openVpnConfigPath;
import servers.connections : Connection;
import servers.protocols   : Protocol;
import servers.countries   : CountryShort;
import command.arguments   : InvalidArgument,
                             MissingValue,
                             InvalidValue,
                             Handlers,
                             translateArguments;


public:

/*----------------------------------------------------------------------------*/
struct Options
{
    string path     = openVpnConfigPath;
    string protocol = Protocol.tcp;
    string port     = "443";
    string connection;
    string target;
    string state;
    string via;
    string free;
    string index;
    string tor;
    string file;
}


/*----------------------------------------------------------------------------*/
void
translateArguments(ref string[] arguments,
                   ref Options  options)
{
    immutable Handlers handlers = {
        tcp: (string[] _)
        {
            options.protocol = Protocol.tcp;
            options.port     = "443";
        },
        udp: (string[] _)
        {
            options.protocol = Protocol.udp;
            options.port     = "1194";
        },
        secureCore: (string[] _)
        {
            options.connection = Connection.secureCore;
        },
        country: (string[] _)
        {
            options.connection = Connection.country;
        },
        server: (string[] _)
        {
            options.connection = Connection.server;
        },
        tor:  (string[] _)
        {
            options.tor = "tor";
        },
        free: (string[] _)
        {
            options.free = "free";
        },
        target: (string[] target)
        {
            options.target = target[0];
        },
        state: (string[] state)
        {
            options.state = state[0];
        },
        via: (string[] via)
        {
            options.via = via[0];
        },
        commandSpecific: (string           argument,
                          Generator!string arguments)
        {
            switch (argument)
            {
                case "--index":
                    arguments.popFront;
                    if (arguments.empty)
                        throw new MissingValue("Missing index for `--index`");
                    try
                    {
                        options.index =
                            format!"%02d"(to!ubyte(arguments.front));
                        break;
                    }
                    catch (ConvException)
                        throw new InvalidValue(
                            "Positive number expected for `--index`");

                case "--path":
                    arguments.popFront;
                    if (arguments.empty)
                        throw new MissingValue("Missing location for `--path`");
                    options.path = arguments.front;
                    break;

                case "--file":
                    arguments.popFront;
                    if (arguments.empty)
                        throw new MissingValue("Missing location for `--file`");
                    options.file = arguments.front;
                    break;

                default:
                    throw new InvalidArgument("Invalid argument: " ~ argument);
            }
        },
    };
    translateArguments(arguments, handlers);

    /* Set smart default values */
    if (options.free &&
        !options.index)
            options.index = "01";

    if (!options.connection)
    {
        if (options.via)
        {
            if (!options.connection)
                options.connection = Connection.secureCore;

            if (!options.index)
                options.index = "01";
        }
        else if (!options.via &&
                 !options.index)
        {
            if (!options.connection)
                options.connection = Connection.country;
        }
        else
            options.connection = Connection.server;
    }

    if (!options.target &&
        options.state)
            options.target = CountryShort.unitedStates;
}
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/+
unittest
{
    import std.exception : assertThrown;

    {
        Options options;
        translateArguments([], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == "country");
        assert (options.target     == null);
        assert (options.via        == null);
        assert (options.free       == null);
        assert (options.index      == null);
        assert (options.tor        == null);
    }
    {
        Options options;
        assertThrown!InvalidArgument(
            translateArguments(["--invalid"], options));
    }
    {
        Options options;
        translateArguments(["--tcp"], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == "country");
        assert (options.target     == null);
        assert (options.via        == null);
        assert (options.index      == null);
        assert (options.tor        == null);
    }
    {
        Options options;
        translateArguments(["--udp"], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "udp");
        assert (options.port       == "1194");
        assert (options.connection == "country");
        assert (options.target     == null);
        assert (options.via        == null);
        assert (options.index      == null);
        assert (options.tor        == null);
    }
    {
        Options options;
        translateArguments(["--secure-core"], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == "secure_core");
        assert (options.target     == null);
        assert (options.via        == null);
        assert (options.index      == null);
        assert (options.tor        == null);
    }
    {
        Options options;
        translateArguments(["--server"], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == "server");
        assert (options.target     == null);
        assert (options.via        == null);
        assert (options.index      == null);
        assert (options.tor        == null);
    }
    {
        Options options;
        translateArguments(["--country"], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == "country");
        assert (options.target     == null);
        assert (options.via        == null);
        assert (options.index      == null);
        assert (options.tor        == null);
    }
    {
        Options options;
        assertThrown!MissingValue(translateArguments(["--target"], options));
    }
    {
        Options options;
        assertThrown!InvalidValue(
            translateArguments(["--target", "invalid"], options));
    }
    {
        Options options;
        translateArguments(["--target", "australia"], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == "country");
        assert (options.target     == "au");
        assert (options.via        == null);
        assert (options.index      == null);
        assert (options.tor        == null);
    }
    {
        Options options;
        assertThrown!MissingValue(translateArguments(["--via"], options));
    }
    {
        Options options;
        assertThrown!InvalidValue(
            translateArguments(["--via", "invalid"], options));
    }
    {
        Options options;
        translateArguments(["--via", "ireland"], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == "secure_core");
        assert (options.target     == null);
        assert (options.via        == "ie");
        assert (options.index      == "01");
        assert (options.tor        == null);
    }
    {
        Options options;
        assertThrown!MissingValue(translateArguments(["--index"], options));
    }
    {
        Options options;
        assertThrown!InvalidValue(
            translateArguments(["--index", "invalid"], options));
    }
    {
        Options options;
        assertThrown!InvalidValue(
            translateArguments(["--index", "-12"], options));
    }
    {
        Options options;
        assertThrown!InvalidValue(
            translateArguments(["--index", to!string(ubyte.max + 1)], options));
    }
    {
        Options options;
        translateArguments(["--index", "1"], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == null);
        assert (options.target     == null);
        assert (options.via        == null);
        assert (options.index      == "01");
        assert (options.tor        == null);
    }
    {
        Options options;
        translateArguments(["--index", "12"], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == null);
        assert (options.target     == null);
        assert (options.via        == null);
        assert (options.index      == "12");
        assert (options.tor        == null);
    }
    {
        Options options;
        translateArguments(["--tor"], options);
        assert (options.path       == openVpnConfigPath);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == "server");
        assert (options.target     == null);
        assert (options.via        == null);
        assert (options.index      == null);
        assert (options.tor        == "tor");
    }
    {
        Options options;
        assertThrown!MissingValue(translateArguments(["--path"], options));
    }
    {
        Options options;
        string  path = "path/to/configs";
        translateArguments(["--path", path], options);
        assert (options.path       == path);
        assert (options.protocol   == "tcp");
        assert (options.port       == "443");
        assert (options.connection == "country");
        assert (options.target     == null);
        assert (options.via        == null);
        assert (options.index      == null);
        assert (options.tor        == null);
    }
    {
        Options options;
        translateArguments(["--udp",
                            "--secure-core",
                            "--target", "poland",
                            "--via", "france",
                            "--index", "1",
                            "--path", "."], options);
        assert (options.path       == ".");
        assert (options.protocol   == "udp");
        assert (options.port       == "1194");
        assert (options.connection == "secure_core");
        assert (options.target     == "pl");
        assert (options.via        == "fr");
        assert (options.index      == "01");
        assert (options.tor        == null);
    }
    {
        Options options;
        assertThrown!MissingValue(translateArguments(["--file"], options));
    }
    {
        Options options;
        string file = "path/to/nowhere.protonvpn.com.udp1194.ovpn";
        translateArguments(["--file", file], options);
        assert (options.file == file);
    }
}
+/
