/* INFO **
** INFO */

module command.connect;

private:

import std.stdio       : stderr,
                         writeln;
import std.path        : expandTilde;
import std.file        : isFile,
                         FileException;
import std.string      : endsWith;
import std.process     : spawnProcess,
                         wait;

import version_                  : version_;
import globals                   : openVpnConfigPath;
import command.help              : handleHelp;
import command.error             : CommandError;
import command.connect.arguments : Options,
                                   translateArguments;


const string help = "
NAME
    protonvpn-connect - Connect to specified ProtonVPN server

SYNOPSIS
    protonvpn connect OPTIONS

OPTIONS
    --tcp
    --udp
        Specify communication protocol.  The default is 'tcp'

    --secure-core
    --country
    --server
        ProtonVPN connection types

    --target country
        Specify target country

    --via country
        Specify intermediate country

    --state state
        Specify state (only if target is US)

    --index index
        Specify server index

    --tor
        Use TOR

    --free
        Use free server

    --path path-to-dir
        Specify alternative directory where the configuration files are

    --file path-to-file
        Specify a full path and file name to an OpenVPN configuration

    --help
        Prints out this text

AUTHORS
    Peter Varo <hello@petervaro.com>

VERSION
    " ~ version_ ~ "

LICENSE
    Copyright (C) 2018-2019 Peter Varo

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANYWARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    this program, most likely a file in the root directory, called 'LICENSE'. If
    not, see http://www.gnu.org/licenses.
";


public:

/*----------------------------------------------------------------------------*/
void
connect(string[] arguments)
{
    handleHelp(arguments, help);

    // FROM HERE ...
    Options options;
    translateArguments(arguments, options);

    string path;

    if (options.file)
        path = options.file;
    else
    {
        char[] server;

        if (!options.target)
            // TODO: If `target` is always required, why is it not a positional
            //       argument then? `$ protonvpn connect australia --via iceland`
            throw new CommandError("Missing `--target`");

        if (options.via)
            server ~= options.via ~ "-";

        server ~= options.target;

        if (options.connection == "server" &&
            options.target == "us" &&
            !options.via &&
            options.state)
                server ~= "-" ~ options.state;

        if (options.free)
            server ~= "-" ~ options.free;

        if (options.index)
            server ~= "-" ~ options.index;

        if (options.tor)
            server ~= "-" ~ options.tor;

        path = expandTilde(options.path ~
                           "/" ~
                           options.protocol ~
                           "/" ~
                           options.connection ~
                           "/" ~
                           server.idup ~
                           ".protonvpn.com." ~
                           options.protocol ~
                           ".ovpn");
    }
    // ... TO HERE: This should be in the `translateArguments` function which
    // should return a string which is a the path to the OVPN file

    try
        path.isFile;
    catch (FileException error)
        if (error.msg.endsWith("Permission denied"))
            throw new CommandError("Configuration file cannot be accessed " ~
                                   "(permission denined): " ~ path);
        else if (error.msg.endsWith("No such file or directory"))
            throw new CommandError(
                "Configuration file does not exist: " ~ path);
        else
            throw new CommandError(
                "Configuration file problem: " ~ error.msg ~ "(" ~ path ~ ")");

    writeln("Connecting to: " ~ path);
    if (spawnProcess(["openvpn", path]).wait)
        throw new CommandError("Cannot connect to: " ~ path);
}
