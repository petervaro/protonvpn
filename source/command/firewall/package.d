/* INFO **
** INFO */

module command.firewall;

private:

import std.array    : join;
import std.stdio    : write,
                      writeln;
import std.typecons : Tuple;
import std.process  : execute;

import version_                   : version_;
import configurations             : Configuration,
                                    getConfigurations,
                                    TraverseError;
import command.help               : handleHelp;
import command.error              : CommandError;
import command.firewall.arguments : translateArguments,
                                    Options;


/* I love D, I really do, but thing like this are driving me crazy.  Which idiot
   thought it would be a good idea not making the return type available of the
   `execute`-family? */
alias Process = Tuple!(int, "status", string, "output");


const string help = r"
NAME
    protonvpn-firewall - Configuring UFW with the correct remote addresses

SYNOPSIS
    protonvpn firewall FILTERS OPTIONS

FILTERS
    --tcp
    --udp
    --secure-core
    --country
    --server
    --tor
    --free
    --target country
    --state state
    --via country
        Filtering attributes

OPTIONS
    --allow RULE
        Specify the IP address, port number, subnet mask, and/or interface name
        the firewall should allow communication through

    --dry
        Dry run, i.e. the command take no effect it just prints to the stdout

    --help
        Prints out this text

RULE
    Each rule can contain any of the following bits, however their order must be
    IP address, port number, subnet mask, and interface name:
        IP address        \d{1,3}(\.\d{1,3}){3}
        Port number       :\d{1,5}
        Subnet mask       /\d{2}
        Interface name    @\w+

EXAMPLE
    protonvpn firewall --allow @tun0 --allow 10.0.1.1@eth0 --allow :80

NOTE
    To find out the router ip address use the following command:
        $ ip route show

    If for some reason the firewall configuration has been interrupted the IP
    table may still have the already applied rules in which case it will
    complain with an error message similar to:
        ERROR: initcaps
        [Errno 2] iptables: Chain already exists.
    To solve this the values should be removed and the configuration process
    can be restarted:
        $ sudo ufw disable
        $ sudo iptables -F
        $ sudo iptables -X
        $ sudo protonvpn firewall <arguments>

AUTHORS
    Peter Varo <hello@petervaro.com>

VERSION
    " ~ version_ ~ "

LICENSE
    Copyright (C) 2018-2019 Peter Varo

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANYWARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    this program, most likely a file in the root directory, called 'LICENSE'. If
    not, see http://www.gnu.org/licenses.
";


/*----------------------------------------------------------------------------*/
void
realRun(ref Options         options,
        ref Configuration[] configurations)
{
    Process process;

    /* Enable firewall */
    if ((process = execute(["ufw", "--force", "disable"])).status)
        throw new CommandError(
            "UFW: Cannot disable firewall\n" ~ process.output);
    writeln(process.output);

    /* Reset all rules */
    if ((process = execute(["ufw", "--force", "reset"])).status)
        throw new CommandError("UFW: Cannot reset firewall\n" ~ process.output);
    writeln(process.output);

    /* Deny everything */
    if ((process = execute(["ufw", "default", "deny", "outgoing"])).status)
        throw new CommandError(
            "UFW: Cannot deny all outgoing communication\n" ~ process.output);
    writeln(process.output);
    if ((process = execute(["ufw", "default", "deny", "incoming"])).status)
        throw new CommandError(
            "UFW: Cannot deny all incoming communication\n" ~ process.output);
    writeln(process.output);

    writeln("Updating rules...");

    /* Allow communication on specific interfaces/IPs/ports/subnets */
    foreach (rule; options.rules)
    {
        if (rule.interface_)
        {
            auto allowed = ["on", rule.interface_];

            if (rule.address)
                allowed ~= ["to", rule.address];

            if (rule.port)
                allowed ~= ["port", rule.port];

            if ((process = execute(["ufw", "allow", "in"] ~ allowed)).status)
                throw new CommandError(
                    "UFW: Cannot allow incoming communication\n" ~
                    process.output);
            if ((process = execute(["ufw", "allow", "out"] ~ allowed)).status)
                throw new CommandError(
                    "UFW: Cannot allow outgoing communication\n" ~
                    process.output);
        }
        else if (rule.address)
        {
            string[] allowed;

            if (rule.subnet)
                allowed ~= rule.address ~ "/" ~ rule.subnet;
            else
                allowed ~= rule.address;

            if (rule.port)
            {
                allowed ~= ["to", "any", "port"];
                allowed ~= rule.port;
            }

            if ((process = execute(["ufw", "allow", "from"] ~ allowed)).status)
                throw new CommandError(
                    "UFW: Cannot allow IP address\n" ~ process.output);
        }
        else if (rule.port)
        {
            if ((process = execute(["ufw", "allow", rule.port])).status)
                throw new CommandError(
                    "UFW: Cannot allow port\n" ~ process.output);
        }
    }

    /* Allow to connect to selected ProtonVPN servers */
    with (options)
        foreach (configuration; configurations)
            if (filter.match(configuration))
                foreach (address; configuration.addresses)
                    if ((process = execute(["ufw", "allow", "out",
                             "to", address.host,
                             "port", address.port,
                             "proto", configuration.protocol])).status)
                        throw new CommandError(
                            "UFW: Unable to allow outgoing communication for " ~
                            configuration.name ~
                            " (" ~ address.host ~ ":" ~ address.port ~ ")\n" ~
                            process.output);

    writeln("Rules have been updated");

    /* Enable firewall */
    if ((process = execute(["ufw", "--force", "enable"])).status)
        throw new CommandError(
            "UFW: Cannot enable firewall\n" ~ process.output);
    writeln(process.output);

    /* Report back to the user */
    if ((process = execute(["ufw", "status", "verbose"])).status)
        throw new CommandError(
            "UFW: Cannot get status of firewall" ~ process.output);
    writeln(process.output);
}


/*----------------------------------------------------------------------------*/
void
dryRun(ref string[] arguments,
       ref Options  options)
{
    writeln("Command:");
    write("\tprotonvpn firewall");
    foreach (argument; arguments)
        if (argument != "--dry")
            write(' ', argument);
    writeln;

    auto hasFilter = false;
    write("Filters:");
    with (options.filter)
    {
        if (protocol)
        {
            write("\n\tprotocol: ", cast(string)protocol);
            hasFilter = true;
        }

        if (connection)
        {
            write("\n\tconnection: ", cast(string)connection);
            hasFilter = true;
        }

        if (tor)
        {
            write("\n\ttor: yes");
            hasFilter = true;
        }

        if (free)
        {
            write("\n\tfree: yes");
            hasFilter = true;
        }

        if (target)
        {
            write("\n\ttarget: ", target);
            hasFilter = true;
        }

        if (state)
        {
            write("\n\tstate: ", state);
            hasFilter = true;
        }

        if (via)
        {
            write("\n\tvia: ", via);
            hasFilter = true;
        }
    }

    if (!hasFilter)
        writeln(" none");
    else
        writeln;

    write("Rules:");
    foreach (i, rule; options.rules)
    {
        writeln("\n\t[", i, ']');
        with (rule)
        {
            if (interface_)
                writeln("\t\tinterface: ", interface_);

            if (address)
                writeln("\t\taddress: ", address);

            if (port)
                writeln("\t\tport: ", port);

            if (subnet)
                writeln("\t\tsubnet: ", subnet);
        }
    }

    if (!options.rules.length)
        writeln(" none");
}


public:

/*----------------------------------------------------------------------------*/
void
firewall(string[] arguments)
{
    handleHelp(arguments, help);

    Configuration[] configurations;
    try
        getConfigurations(configurations);
    catch (TraverseError error)
        throw new CommandError(error.msg);

    Options options;
    translateArguments(arguments, options);

    if (options.dry)
        dryRun(arguments, options);
    else
        realRun(options, configurations);
}
