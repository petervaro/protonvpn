/* INFO **
** INFO */

module command.firewall.arguments;

private:

import std.regex       : ctRegex,
                         matchFirst;
import std.concurrency : Generator;

import configurations      : Configuration;
import servers.connections : Connection;
import servers.protocols   : Protocol;
import command.arguments   : InvalidArgument,
                             MissingValue,
                             InvalidValue,
                             Handlers,
                             translateArguments;

const rulePattern = ctRegex!(
    r"(?P<address>\d{1,3}(\.\d{1,3}){3})?"  ~
    r"(:(?P<port>\d{1,5}))?"                ~
    r"(/(?P<subnet>\d\d))?"                 ~
    r"(@(?P<interface>\w+))?");

public:

/*----------------------------------------------------------------------------*/
struct Rule
{
    string interface_;
    string address;
    string port;
    string subnet;
}


/*----------------------------------------------------------------------------*/
struct Options
{
    Configuration filter;
    Rule[]        rules;
    bool          dry;
}


/*----------------------------------------------------------------------------*/
void
translateArguments(ref string[] arguments,
                   ref Options  options)
{
    immutable Handlers handlers = {
        dry: (string[] _)
        {
            options.dry = true;
        },
        tcp: (string[] _)
        {
            options.filter.protocol = Protocol.tcp;
        },
        udp: (string[] _)
        {
            options.filter.protocol = Protocol.udp;
        },
        secureCore: (string[] _)
        {
            options.filter.connection = Connection.secureCore;
        },
        country: (string[] _)
        {
            options.filter.connection = Connection.country;
        },
        server: (string[] _)
        {
            options.filter.connection = Connection.server;
        },
        tor:  (string[] _)
        {
            options.filter.tor = "tor";
        },
        free: (string[] _)
        {
            options.filter.free = "free";
        },
        target: (string[] target)
        {
            options.filter.target = target[0];
        },
        state: (string[] state)
        {
            options.filter.state = state[0];
        },
        via: (string[] via)
        {
            options.filter.via = via[0];
        },
        commandSpecific: (string           argument,
                          Generator!string arguments)
        {
            if (argument != "--allow")
                throw new InvalidArgument("Invalid argument: " ~ argument);

            arguments.popFront;
            if (arguments.empty)
                throw new MissingValue("Missing rule for `--allow`");

            auto match = arguments.front.matchFirst(rulePattern);
            if (!match)
                throw new InvalidValue("Invalid rule for `--allow`");

            auto iface   = match["interface"];
            auto address = match["address"];
            auto port    = match["port"];
            auto subnet  = match["subnet"];

            options.rules ~= Rule(
                iface && iface.length ? iface : null,
                address && address.length ? address : null,
                port && port.length ? port : null,
                subnet && subnet.length ? subnet : null);
        },
    };

    translateArguments(arguments, handlers);
}
