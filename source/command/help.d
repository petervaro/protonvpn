/* INFO **
** INFO */

module command.help;

private:

import std.stdio : writeln;

import error : ErrorConstructor;

public:

/*----------------------------------------------------------------------------*/
class HelpArgumentDetected: Exception
{
    mixin ErrorConstructor;
}


/*----------------------------------------------------------------------------*/
void
handleHelp(string[] arguments,
           string   help)
{
    foreach (argument; arguments)
        if (argument == "help"  ||
            argument == "-h"    ||
            argument == "--h"   ||
            argument == "-help" ||
            argument == "--help")
                throw new HelpArgumentDetected(help);
}
