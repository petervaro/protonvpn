/* INFO **
** INFO */

module command.query;

private:

import std.stdio : writeln;
import std.array : join;
import std.conv  : to;
import std.uni   : toUpper;

import version_                : version_;
import servers.countries       : countryShortToHumanReadable,
                                 countryShortToLong;
import servers.connections     : Connection;
import servers.protocols       : Protocol;
import servers.states          : stateShortToHumanReadable;
import configurations          : Configuration,
                                 getConfigurations,
                                 TraverseError;
import command.help            : handleHelp;
import command.error           : CommandError;
import command.query.arguments : translateArguments;


const string help = "
NAME
    protonvpn-query - Lists the filtered ProtonVPN servers

SYNOPSIS
    protonvpn query FILTERS

FILTERS
    --tcp
    --udp
    --secure-core
    --country
    --server
    --tor
    --free
    --target country
    --state state
    --via country
        Filtering attributes

    --help
        Prints out this text

EXAMPLE
    Print out all the TOR enabled servers:
        $ protonvpn query --tor

    Print out all the secure core servers which are supporting the TCP protocol
    and connected to their target servers via Switzerland:
        $ protonvpn query --secure-core --tcp --via switzerland

AUTHORS
    Peter Varo <hello@petervaro.com>

VERSION
    " ~ version_ ~ "

LICENSE
    Copyright (C) 2018-2019 Peter Varo

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANYWARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    this program, most likely a file in the root directory, called 'LICENSE'. If
    not, see http://www.gnu.org/licenses.
";


public:

/*----------------------------------------------------------------------------*/
void
query(string[] arguments)
{
    handleHelp(arguments, help);

    Configuration filter;
    translateArguments(arguments, filter);

    Configuration[] configurations;
    try
        getConfigurations(configurations);
    catch (TraverseError error)
        throw new CommandError(error.msg);

    size_t resultCount;
    foreach (configuration; configurations)
        if (filter.match(configuration))
        {
            ++resultCount;

            string[] info = ["\033[33m*\033[39m A"];
            string[] connect = ["    sudo -E protonvpn connect"];
            string   temp;

            if (configuration.free)
            {
                info ~= "free";
                connect ~= "--free";
            }

            switch (configuration.connection)
            {
                case Connection.secureCore:
                    info ~= "secure core server";
                    connect ~= "--secure-core";
                    break;

                case Connection.country:
                    info ~= "recommended country server";
                    connect ~= "--country";
                    break;

                case Connection.server:
                    info ~= "specific server";
                    connect ~= "--server";
                    break;

                default:
                    assert (false);
            }

            info ~= "in";

            if (configuration.state)
            {
                stateShortToHumanReadable(configuration.state, temp);
                info ~= temp ~ ",";
                connect ~= "--state";
                connect ~= temp;
            }

            countryShortToHumanReadable(configuration.target, temp);
            info ~= temp;
            countryShortToLong(configuration.target, temp);
            connect ~= "--target";
            connect ~= temp;


            if (configuration.via)
            {
                countryShortToHumanReadable(configuration.via, temp);
                info ~= "via";
                info ~= temp;
                countryShortToLong(configuration.via, temp);
                connect ~= "--via";
                connect ~= temp;
            }

            if (configuration.protocol)
            {
                info ~= "using";
                info ~= configuration.protocol.toUpper;
                info ~= "protocol";
                switch (configuration.protocol)
                {
                    case Protocol.tcp:
                        connect ~= "--tcp";
                        break;

                    case Protocol.udp:
                        connect ~= "--udp";
                        break;

                    default:
                        assert (false);
                }
            }

            if (configuration.tor)
            {
                info ~= "over tor";
                connect ~= "--tor";
            }

            if (configuration.index)
            {
                connect ~= "--index";
                connect ~= configuration.index;
            }

            writeln("\033[1m", info.join(" "), ":\033[0m");
            writeln(connect.join(" "), "\n");
        }

    writeln(resultCount.to!string ~
            " result" ~ (resultCount == 1 ? "" : "s") ~
            " found");
}
