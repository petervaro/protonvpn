/* INFO **
** INFO */

module command.query.arguments;

private:

import servers.connections : Connection;
import servers.protocols   : Protocol;
import configurations      : Configuration;
import command.arguments   : Handlers,
                             translateArguments;

public:

/*----------------------------------------------------------------------------*/
void
translateArguments(ref string[]      arguments,
                   ref Configuration configuration)
{
    // TODO: Add here the same 'smart defaults' as in the `connect` command
    immutable Handlers handlers = {
        tcp: (string[] _)
        {
            configuration.protocol = Protocol.tcp;
        },
        udp: (string[] _)
        {
            configuration.protocol = Protocol.udp;
        },
        secureCore: (string[] _)
        {
            configuration.connection = Connection.secureCore;
        },
        country: (string[] _)
        {
            configuration.connection = Connection.country;
        },
        server: (string[] _)
        {
            configuration.connection = Connection.server;
        },
        tor:  (string[] _)
        {
            configuration.tor = "tor";
        },
        free: (string[] _)
        {
            configuration.free = "free";
        },
        target: (string[] target)
        {
            configuration.target = target[0];
        },
        state: (string[] state)
        {
            configuration.state = state[0];
        },
        via: (string[] via)
        {
            configuration.via = via[0];
        },
    };
    translateArguments(arguments, handlers);
}
