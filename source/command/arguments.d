/* INFO **
** INFO */

module command.arguments;

private:

import std.concurrency : Generator,
                         yield;

import error             : ErrorConstructor;
import servers.error     : ServerError;
import servers.countries : countryLongToShort;
import servers.states    : stateLongToShort;
import configurations    : Configuration;
import command.error     : CommandError;

public:

/*----------------------------------------------------------------------------*/
class InvalidArgument: CommandError { mixin ErrorConstructor; }
/*----------------------------------------------------------------------------*/
class MissingValue: CommandError { mixin ErrorConstructor; }
/*----------------------------------------------------------------------------*/
class InvalidValue: CommandError { mixin ErrorConstructor; }


/*----------------------------------------------------------------------------*/
pragma (inline)
Generator!string
ArgumentGenerator(ref string[] arguments)
{
    return new Generator!string({
        foreach (argument; arguments) yield(argument);
    });
}


/*----------------------------------------------------------------------------*/
alias GeneralHandler = void delegate (string[] values);


/*----------------------------------------------------------------------------*/
alias SpecialHandler = void delegate (string            option,
                                      Generator!string  arguments);


/*----------------------------------------------------------------------------*/
struct Handlers
{
    GeneralHandler dry;
    GeneralHandler tcp;
    GeneralHandler udp;
    GeneralHandler secureCore;
    GeneralHandler country;
    GeneralHandler server;
    GeneralHandler tor;
    GeneralHandler free;
    GeneralHandler target;
    GeneralHandler state;
    GeneralHandler via;
    SpecialHandler commandSpecific;
}


/*----------------------------------------------------------------------------*/
void
translateArguments(ref string[] args,
                   Handlers     handlers)
{
    auto arguments = ArgumentGenerator(args);
    with (handlers)
        foreach (argument; arguments)
            switch (argument)
            {
                case "--dry":
                    if (!dry)
                        goto invalidArgument;
                    dry(null);
                    break;

                case "--tcp":
                    if (!tcp)
                        goto invalidArgument;
                    tcp(null);
                    break;

                case "--udp":
                    if (!udp)
                        goto invalidArgument;
                    udp(null);
                    break;

                case "--secure-core":
                    if (!secureCore)
                        goto invalidArgument;
                    secureCore(null);
                    break;

                case "--country":
                    if (!country)
                        goto invalidArgument;
                    country(null);
                    break;

                case "--server":
                    if (!server)
                        goto invalidArgument;
                    server(null);
                    break;

                case "--tor":
                    if (!tor)
                        goto invalidArgument;
                    tor(null);
                    break;

                case "--free":
                    if (!free)
                        goto invalidArgument;
                    free(null);
                    break;

                case "--target":
                {
                    if (!target)
                        goto invalidArgument;
                    arguments.popFront;
                    if (arguments.empty)
                        throw new MissingValue(
                            "Missing country for `--target`");
                    string value;
                    try
                    {
                        countryLongToShort(arguments.front, value);
                        target([value]);
                        break;
                    }
                    catch (ServerError error)
                        throw new InvalidValue(error.msg);
                }

                case "--state":
                {
                    if (!state)
                        goto invalidArgument;
                    arguments.popFront;
                    if (arguments.empty)
                        throw new MissingValue("Missing state for `--state`");

                    string value;
                    try
                    {
                        stateLongToShort(arguments.front, value);
                        state([value]);
                        break;
                    }
                    catch (ServerError error)
                        throw new InvalidValue(error.msg);
                }

                case "--via":
                {
                    if (!via)
                        goto invalidArgument;
                    arguments.popFront;
                    if (arguments.empty)
                        throw new MissingValue("Missing country for `--via`");

                    string value;
                    try
                    {
                        countryLongToShort(arguments.front, value);
                        via([value]);
                        break;
                    }
                    catch (ServerError error)
                        throw new InvalidValue(error.msg);
                }

                default:
                    if (!commandSpecific)
                        goto invalidArgument;
                    commandSpecific(argument, arguments);
                    break;

                invalidArgument:
                    throw new InvalidArgument("Invalid argument: " ~ argument);
            }

}
