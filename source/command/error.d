/* INFO **
** INFO */

module command.error;

private:

import error : ErrorConstructor;

public:

/*----------------------------------------------------------------------------*/
class CommandError: Exception
{
    mixin ErrorConstructor;
}
