/* INFO **
** INFO */

import std.stdio        : stderr,
                          writeln,
                          File;
import std.string       : strip;
import core.stdc.stdlib : exitSuccess = EXIT_SUCCESS,
                          exitFailure = EXIT_FAILURE;

import version_         : version_;
import command.help     : HelpArgumentDetected;
import command.error    : CommandError;
import command.connect  : connect;
import command.firewall : firewall;
import command.query    : query;

string help = "
NAME
    protonvpn - ProtonVPN manager

SYNOPSIS
    protonvpn COMMAND OPTIONS
    protonvpn [-h | --help]
    protonvpn [-v | --version]

COMMAND
    For more information and available options for each subcommand use the `-h`
    or `--help` with each subcommand, e.g.
        $ protonvpn firewall --help

    firewall OPTIONS
        Configuring UFW

    connect OPTIONS
        Connect to specified server

    query OPTIONS
        Lists the filtered VPN servers

    help
        Prints out this text

EXAMPLE
    $ protonvpn firewall --allow @wlp6s0 --allow 192.168.1.0/24:80
    $ protonvpn query --via united-kingdom --tor
    $ protonvpn connect --target denmark
    $ protonvpn connect --tcp --secure-core --target italy --via spain --index 1

AUTHORS
    Peter Varo <hello@petervaro.com>

VERSION
    " ~ version_ ~ "

LICENSE
    Copyright (C) 2018-2019 Peter Varo

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANYWARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with
    this program, most likely a file in the root directory, called 'LICENSE'. If
    not, see http://www.gnu.org/licenses.
";


/*----------------------------------------------------------------------------*/
int
main(string[] args)
{
    /* If no arguments has passed */
    if (args.length <= 1)
        goto help;

    try
        switch (args[1])
        {
            case "connect":
                connect(args[2..$]);
                break;

            case "firewall":
                firewall(args[2..$]);
                break;

            case "query":
                query(args[2..$]);
                break;

            // case "update":  // updates openvpn configuration files
            // case "download":
            //     break;

            case "-v":
            case "--version":
                writeln(version_);
                break;

            case "help":
            case "-h":
            case "--h":
            case "-help":
            case "--help":
                goto help;

            default:
                throw new CommandError("Invalid subcommand, try 'help'");
        }
    catch (HelpArgumentDetected error)
    {
        writeln(error.msg);
        return exitSuccess;
    }
    catch (CommandError error)
    {
        stderr.writeln(error.msg);
        return exitFailure;
    }

    return exitSuccess;

    help:
        writeln(help);
        return exitSuccess;
}
