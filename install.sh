## INFO ##
## INFO ##

printf "Compiling binary...\n";
dub build --compiler=ldc --build=release;
if [ $? -eq 0 ];
then
    printf "Installing compiled binary...\n";
    sudo cp protonvpn /usr/local/bin;
fi;
